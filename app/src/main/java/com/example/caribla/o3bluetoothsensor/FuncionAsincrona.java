package com.example.caribla.o3bluetoothsensor;

import android.os.AsyncTask;
import android.util.Log;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class FuncionAsincrona extends AsyncTask<Void, Void, Void> {

    private int valorMedicion;
    private double latitud;
    private double longitud;
    private String tipoMedidaObtenida;

    public FuncionAsincrona (int valor, double lat, double longi, String tipoMedida){

        valorMedicion = valor;
        latitud = lat;
        longitud = longi;
        tipoMedidaObtenida = tipoMedida;

    }

    @Override
    protected Void doInBackground(Void... voids) {

        try{
            //JSONObject json = new JSONObject().put("valor", valorMedicion).put("lat", latitud).put("lng", longitud).put("tipoMedida", tipoMedidaObtenida);

            List<NameValuePair> parametros = new ArrayList<>();
            parametros.add(new BasicNameValuePair("valor",Integer.toString(valorMedicion)));
            parametros.add(new BasicNameValuePair("lat", Double.toString(latitud)));
            parametros.add(new BasicNameValuePair("lng", Double.toString(longitud)));
            parametros.add(new BasicNameValuePair("tipoMedida", tipoMedidaObtenida));
            parametros.add(new BasicNameValuePair("mail", "mailpruebas@gmail.com"));

            HttpClient client = new DefaultHttpClient();
            HttpPost method = new HttpPost("http://192.168.43.115/_Sprint0/api/v1.0/mediciones");

            method.setEntity(new UrlEncodedFormEntity(parametros));
            //method.setEntity(new StringEntity(json.toString(),"utf-8"));
            client.execute(method);
            Log.d("Enviado","Se ha subido a la BBDD");
        } catch (Exception e){
            Log.d(e.toString(), "Excepcion");
            e.printStackTrace();
        }

        return null;
    }

    @Override protected void onPreExecute(){super.onPreExecute();}
}
