package com.example.caribla.o3bluetoothsensor;

// ------------------------------------------------------------------
// ------------------------------------------------------------------
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.icu.util.EthiopicCalendar;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

// ------------------------------------------------------------------
// ------------------------------------------------------------------

public class MainActivity extends AppCompatActivity {

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private static String ETIQUETA_LOG = ">>>>";

    private BluetoothAdapter.LeScanCallback  callbackLeScan;

    private Integer medicion;

    private Integer medicionEnvio;

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private FusedLocationProviderClient fusedLocationProviderClient;

    private Location localizacion;

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private int contadorBeacon = 0;

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private void buscarTodosLosDispositivosBTLE() {
        this.callbackLeScan = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(BluetoothDevice bluetoothDevice, int rssi, byte[] bytes) {

                //  se ha encontrado un dispositivo

                // Esta linea sirve para mostrar los datos de los Beacons encontrados (comentado debido a que hay muchos dispositivos)
                // mostrarInformacionDispositivoBTLE( bluetoothDevice, rssi, bytes );
                TramaIBeacon nuevoBeacon = new TramaIBeacon(bytes);

                haLlegadoNuevoBeacon(nuevoBeacon);

            } // onLeScan()
        }; // new LeScanCallback

        //
        //
        //
        boolean resultado = BluetoothAdapter.getDefaultAdapter().startLeScan( this.callbackLeScan );

        // Log.d(ETIQUETA_LOG, " buscarTodosLosDispositivosBTL(): startLeScan(), resultado= " + resultado );
    } // ()

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private void haLlegadoNuevoBeacon(TramaIBeacon nuevoBeacon) {

        medicion = extraerMediciones(nuevoBeacon, contadorBeacon);
        Log.d(String.valueOf(medicion), "Valor medicion");

        if ( medicion != null ){
            medicionEnvio = medicion;
            solicitarUbicacionUsuario();
        } else {
            Log.d(ETIQUETA_LOG, "Beacon que no es el nuestro o valor antiguo medido por el usuario");
        }

    }

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private Integer extraerMediciones(TramaIBeacon beacon, int ultimoContadorVisto){

        byte[] major = beacon.getMajor();

        byte[] idSensor = Arrays.copyOfRange(major,0,1);
        byte[] contador = Arrays.copyOfRange(major,1,2);

        Log.d(Integer.toString(Utilidades.bytesToInt(idSensor)), "Comprobar ID del sensor");

        if (Utilidades.bytesToInt(idSensor) == 111 && ultimoContadorVisto != Utilidades.bytesToInt(contador)){
            String text = Utilidades.bytesToString(beacon.getUUID());
            ultimoContadorVisto = Utilidades.bytesToInt(contador);
            contadorBeacon = ultimoContadorVisto;
            //Log.d( text , "UUID del Beacon");
            return Utilidades.bytesToInt(beacon.getMinor());
        } else {
            return null;
        }

    }

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private void enviarMedicion(Integer valor, double lat, double longi){
        Log.d(ETIQUETA_LOG, "Enviando");
        new FuncionAsincrona(valor.intValue(), lat, longi, "Ozono").execute();
    }

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private void mostrarInformacionDispositivoBTLE( BluetoothDevice bluetoothDevice, int rssi, byte[] bytes) {

        Log.d(ETIQUETA_LOG, " ****************************************************");
        Log.d(ETIQUETA_LOG, " ************ DISPOSITIVO DETECTADO BTLE ************ ");
        Log.d(ETIQUETA_LOG, " ****************************************************");
        Log.d(ETIQUETA_LOG, " nombre = " + bluetoothDevice.getName());
        Log.d(ETIQUETA_LOG, " dirección = " + bluetoothDevice.getAddress());
        Log.d(ETIQUETA_LOG, " rssi = " + rssi );

        Log.d(ETIQUETA_LOG, " bytes = " + new String(bytes));
        Log.d(ETIQUETA_LOG, " bytes (" + bytes.length + ") = " + Utilidades.bytesToHexString(bytes));

        TramaIBeacon tib = new TramaIBeacon(bytes);

        Log.d(ETIQUETA_LOG, " ----------------------------------------------------");
        Log.d(ETIQUETA_LOG, " prefijo  = " + Utilidades.bytesToHexString(tib.getPrefijo()));
        Log.d(ETIQUETA_LOG, " advFlags = " + Utilidades.bytesToHexString(tib.getAdvFlags()));
        Log.d(ETIQUETA_LOG, " advHeader = " + Utilidades.bytesToHexString(tib.getAdvHeader()));
        Log.d(ETIQUETA_LOG, " companyID = " + Utilidades.bytesToHexString(tib.getCompanyID()));
        Log.d(ETIQUETA_LOG, " iBeacon type = " + Integer.toHexString(tib.getiBeaconType()));
        Log.d(ETIQUETA_LOG, " iBeacon length 0x = " + Integer.toHexString(tib.getiBeaconLength()) + " ( " + tib.getiBeaconLength() + " ) ");
        Log.d(ETIQUETA_LOG, " uuid  = " + Utilidades.bytesToHexString(tib.getUUID()));
        Log.d(ETIQUETA_LOG, " uuid  = " + Utilidades.bytesToString(tib.getUUID()));
        Log.d(ETIQUETA_LOG, " major  = " + Utilidades.bytesToHexString(tib.getMajor()) + "( " + Utilidades.bytesToInt(tib.getMajor()) + " ) ");
        Log.d(ETIQUETA_LOG, " minor  = " + Utilidades.bytesToHexString(tib.getMinor()) + "( " + Utilidades.bytesToInt(tib.getMinor()) + " ) ");
        Log.d(ETIQUETA_LOG, " txPower  = " + Integer.toHexString(tib.getTxPower()) + " ( " + tib.getTxPower() + " )");
        Log.d(ETIQUETA_LOG, " ****************************************************");

    } // ()

    // --------------------------------------------------------------
    // --------------------------------------------------------------

    private void buscarEsteDispositivoBTLE(final UUID dispositivoBuscado ) {
        Log.d(ETIQUETA_LOG + ":  " + dispositivoBuscado.toString(), "UUID Beacon encontrado");
        this.callbackLeScan = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(BluetoothDevice bluetoothDevice, int rssi, byte[] bytes) {

                //
                // dispostivo encontrado
                //

                TramaIBeacon tib = new TramaIBeacon( bytes );
                String uuidString =  Utilidades.bytesToString( tib.getUUID() );

                Log.d(ETIQUETA_LOG + ":  " + uuidString, "UUID String Beacon encontrado");

                if ( uuidString.compareTo( Utilidades.uuidToString( dispositivoBuscado ) ) == 0 )  {
                    detenerBusquedaDispositivosBTLE();
                    mostrarInformacionDispositivoBTLE( bluetoothDevice, rssi, bytes );


                } else {
                    Log.d( MainActivity.ETIQUETA_LOG, " * UUID buscado >" +
                            Utilidades.uuidToString( dispositivoBuscado ) + "< no concuerda con este uuid = >" + uuidString + "<");
                }

            } // onLeScan()
        }; // new LeScanCallback

        //
        //
        //
        BluetoothAdapter.getDefaultAdapter().startLeScan( this.callbackLeScan );
    } // ()

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private void detenerBusquedaDispositivosBTLE() {
        if ( this.callbackLeScan == null ) {
            return;
        }

        //
        //
        //
        BluetoothAdapter.getDefaultAdapter().stopLeScan(this.callbackLeScan);
        this.callbackLeScan = null;
    } // ()

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private void solicitarUbicacionUsuario(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            Log.d(ETIQUETA_LOG, "Solicitando ubicacion");

            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override public void onComplete(@NonNull Task<Location> task) {
                    localizacion = task.getResult();
                    if(localizacion != null){
                        try {
                            Geocoder geocoder = new Geocoder (MainActivity.this, Locale.getDefault());
                            List<Address> direccion = geocoder.getFromLocation(localizacion.getLatitude(), localizacion.getLongitude(),1);
                            Log.d(ETIQUETA_LOG, "He acabado ubicacion");
                            //Log.d(medicionEnvio.toString(), "Test valor correcto");
                            enviarMedicion(medicionEnvio, direccion.get(0).getLatitude(), direccion.get(0).getLongitude());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    }

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    public void botonBuscarDispositivosBTLEPulsado( View v ) {
        Log.d(ETIQUETA_LOG, " Boton buscar dispositivos BTLE Pulsado" );
        this.detenerBusquedaDispositivosBTLE();
        this.buscarTodosLosDispositivosBTLE();
    } // ()

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        this.buscarTodosLosDispositivosBTLE();

    } // onCreate()

} // class
// --------------------------------------------------------------
// --------------------------------------------------------------
// --------------------------------------------------------------
// --------------------------------------------------------------
